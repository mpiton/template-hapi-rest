import { Db } from "mongodb";

interface RequestWithMongo extends Request {
  mongo: {
    db: Db;
  };
}

export { RequestWithMongo };
