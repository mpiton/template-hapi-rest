import { Server } from "@hapi/hapi";
import { expect } from "chai";
import { afterEach, beforeEach, describe, it } from "mocha";

import { init } from "../src/server";

describe("Server is up", () => {
	let server: Server;

	beforeEach(async () => {
		server = await init();
	});

	afterEach(async () => {
		await server.stop();
	});

	it("should return 200", async () => {
		const response = await server.inject({
			method: "GET",
			url: "/",
		});

		expect(response.statusCode).to.equal(200);
	});
});

describe("Get ALL Feeds", () => {
	let server: Server;

	beforeEach(async () => {
		server = await init();
	});

	afterEach(async () => {
		await server.stop();
	});

	it("should return 200", async () => {
		const response = await server.inject({
			method: "GET",
			url: "/feeds",
		});

		expect(response.statusCode).to.equal(200);
		expect(response.result).to.be.an("array");
	});
});
